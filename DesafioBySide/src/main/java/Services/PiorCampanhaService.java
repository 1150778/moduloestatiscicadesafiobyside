package Services;

import MainMenu.Main;
import scriptella.execution.EtlExecutor;
import scriptella.execution.EtlExecutorException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Paths;

public class PiorCampanhaService {

    private final String FILE_NAME = "xml/PiorCampanha.xml";

    public PiorCampanhaService() {
    }

    public void PiorCampanha()  {
        String csvFile="";

        ClassLoader classLoader = new Main().getClass().getClassLoader();

        File file = new File(classLoader.getResource(FILE_NAME).getFile());

        try {
            EtlExecutor.newExecutor(file).execute();
        }catch(EtlExecutorException e){
            e.printStackTrace();
        }

        try {
            csvFile = Paths.get("target/classes/xml", "/PiorCampanha.csv").toUri().toURL().getFile();
        }catch(MalformedURLException e){
            e.printStackTrace();
        }

        String line = "";
        String cvsSplitBy = ",";

        try (BufferedReader reader = new BufferedReader(new FileReader(csvFile))) {

            while ((line = reader.readLine()) != null) {

                String[] result = line.split(cvsSplitBy);

                System.out.println("A Pior Campanha é a Campanha " + result[0] + " com o id " + result[1]);
                break;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
