package Services;

import MainMenu.Main;
import scriptella.execution.EtlExecutor;
import scriptella.execution.EtlExecutorException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Paths;

public class TaxaSucessoReenviosService {
    private final String FILE_NAME = "xml/TaxaSucessoReenvios.xml";

    public TaxaSucessoReenviosService() {
    }

    public void TaxaSucessoReenvios()  {
        String csvFile="";

        ClassLoader classLoader = new Main().getClass().getClassLoader();

        File file = new File(classLoader.getResource(FILE_NAME).getFile());

        try {
            EtlExecutor.newExecutor(file).execute();
        }catch(EtlExecutorException e){
            e.printStackTrace();
        }

        try {
            csvFile = Paths.get("target/classes/xml", "/TaxaSucessoReenvios.csv").toUri().toURL().getFile();
        }catch(MalformedURLException e){
            e.printStackTrace();
        }

        String line = "";
        String cvsSplitBy = ",";

        try (BufferedReader reader = new BufferedReader(new FileReader(csvFile))) {

            while ((line = reader.readLine()) != null) {

                String[] result = line.split(cvsSplitBy);

                System.out.println("A Campanha com o id " + result[0] + " tem a maior taxa de sucesso de reenvios com: " + result[1] +"%");
                break;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
