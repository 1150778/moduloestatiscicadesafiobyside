package Services;

import MainMenu.Main;
import scriptella.execution.EtlExecutor;
import scriptella.execution.EtlExecutorException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Paths;

public class CampanhaEnviadaMaisPaisesServices {
    private final String FILE_NAME = "xml/CampanhaEnviadaMaisPaises.xml";

    public CampanhaEnviadaMaisPaisesServices() {
    }

    public void CampanhaEnviadaMaisPaises()  {
        String csvFile="";

        ClassLoader classLoader = new Main().getClass().getClassLoader();

        File file = new File(classLoader.getResource(FILE_NAME).getFile());

        try {
            EtlExecutor.newExecutor(file).execute();
        }catch(EtlExecutorException e){
            e.printStackTrace();
        }

        try {
            csvFile = Paths.get("target/classes/xml", "/CampanhaEnviadaMaisPaises.csv").toUri().toURL().getFile();
        }catch(MalformedURLException e){
            e.printStackTrace();
        }

        String line = "";
        String cvsSplitBy = ",";
        int lastvalue=0;
        try (BufferedReader reader = new BufferedReader(new FileReader(csvFile))) {

            while ((line = reader.readLine()) != null) {

                String[] result = line.split(cvsSplitBy);
                result[1]=result[1].replace("\"","");
                int count = Integer.parseInt(result[1]);
                if(count<lastvalue){
                    break;
                }
                System.out.println("A Campanha " + result[0] + " com o id " + result[1] +" foi enviada para " + result[2] + " paises");
                lastvalue=count;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
