
-- MySQL Server version	 5.7.27-30-log

SET NAMES utf8;
SET SESSION FOREIGN_KEY_CHECKS = 0;

-- ------------------------------------------------------------
-- Dump of table country
-- ------------------------------------------------------------

DROP TABLE IF EXISTS `country`;

CREATE TABLE `country` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `iso_code` char(2) NOT NULL COMMENT 'Country alpha-2 ISO-3166-1 code',
  `mcc` smallint(3) NOT NULL COMMENT 'Mobile Country Code, a 3-digit international country unique identifier',
  `phone_prefix` smallint(3) unsigned NOT NULL COMMENT 'Country international phone prefix (without +)',
  `db_create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_lastupdate_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds data about countries';

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;

INSERT INTO `country` (`id`, `name`, `iso_code`, `mcc`, `phone_prefix`)
VALUES
	(1,'Portugal','PT',268,351),
	(2,'Spain','ES',214,34),
	(3,'France','FR',208,33),
	(4,'Germany','DE',262,49),
	(5,'Greece','GR',202,30),
	(6,'Italy','IT',222,39),
	(7,'Netherlands','NL',204,31),
	(8,'United Kingdom','GB',234,44),
	(9,'Austria','AT',232,43),
	(10,'Belgium','BE',206,32),
	(11,'Croatia','HR',219,385),
	(12,'Czech Republic','CZ',230,420),
	(13,'Denmark','DK',238,45),
	(14,'Finland','FI',244,358),
	(15,'Iceland','IS',274,354),
	(16,'Hungary','HU',216,36),
	(17,'Poland','PL',260,48),
	(18,'Norway','NO',242,47),
	(19,'Romania','RO',226,40),
	(20,'Slovakia','SK',231,421),
	(21,'Slovenia','SI',293,386),
	(22,'Switzerland','CH',228,41),
	(23,'Sweden','SE',240,46),
	(24,'Turkey','TR',286,90),
	(25,'Ukraine','UA',255,380),
	(26,'Serbia','RS',220,381),
	(27,'Russian Federation','RU',250,79),
	(28,'Montenegro','ME',297,382),
	(29,'Malta','MT',278,356),
	(30,'Macedonia','MK',294,389),
	(31,'Luxembourg','LU',270,352),
	(32,'Lithuania','LT',246,370),
	(33,'Liechtenstein','LI',295,423),
	(34,'Moldova','MD',259,373),
	(35,'Latvia','LV',247,371),
	(36,'Ireland','IE',272,353),
	(37,'Georgia','GE',282,995),
	(38,'Estonia','EE',248,372),
	(39,'Bulgaria','BG',284,359),
	(40,'Bosnia & Herzegovina','BA',218,387),
	(41,'Belarus','BY',257,375),
	(42,'Armenia','AM',283,374),
	(43,'Andorra','AD',213,376),
	(44,'Albania','AL',276,355);

/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

-- ------------------------------------------------------------
-- Dump of table mobile_network
-- ------------------------------------------------------------

DROP TABLE IF EXISTS `mobile_network`;

CREATE TABLE `mobile_network` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `mnc` char(2) NOT NULL COMMENT 'Mobile Network Code, a 2-digit international network identifier, used in conjunction with MCC',
  `country_id` int(10) unsigned NOT NULL,
  `db_create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_lastupdate_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_country` (`country_id`),
  CONSTRAINT `mobile_network_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds data about mobile network networks';

LOCK TABLES `mobile_network` WRITE;
/*!40000 ALTER TABLE `mobile_network` DISABLE KEYS */;

INSERT INTO `mobile_network` (`id`, `name`, `mnc`, `country_id`)
VALUES
	(1,'Vodafone PT','01',1),
	(2,'NOS comunicações','03',1),
	(3,'MEO','06',1),
	(4,'Vodafone ES','01',2),
	(5,'Movistar','05',2),
	(6,'Orange','03',2);

/*!40000 ALTER TABLE `mobile_network` ENABLE KEYS */;
UNLOCK TABLES;

-- ------------------------------------------------------------
-- Dump of table service_provider
-- ------------------------------------------------------------

DROP TABLE IF EXISTS `service_provider`;

CREATE TABLE `service_provider` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_name` varchar(255) NOT NULL DEFAULT '',
  `account_name` varchar(255) NOT NULL DEFAULT '',
  `account_username` varchar(255) NOT NULL DEFAULT '',
  `account_password` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `db_create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_lastupdate_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds data about the available SMS service providers';

LOCK TABLES `service_provider` WRITE;
/*!40000 ALTER TABLE `service_provider` DISABLE KEYS */;

INSERT INTO `service_provider` (`id`, `service_name`, `account_name`, `account_username`, `account_password`, `url`)
VALUES
	(1,'SMSAPI','BySide','smsuser','smspassword','https://www.smsapi.com/'),
	(2,'Twillio','BySide','smsuser','smspassword','https://www.twilio.com/'),
	(3,'InfoBip','BySide','smsuser','smspassword','https://www.infobip.com/'),
	(4,'Nexmo','BySide','smsuser','smspassword','https://www.nexmo.com/'),
	(5,'ClickSend','BySide','smsuser','smspassword','https://www.clicksend.com/');

/*!40000 ALTER TABLE `service_provider` ENABLE KEYS */;
UNLOCK TABLES;

-- ------------------------------------------------------------
-- Dump of table sms_campaign
-- ------------------------------------------------------------

DROP TABLE IF EXISTS `sms_campaign`;

CREATE TABLE `sms_campaign` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `type_id` int(10) unsigned NOT NULL,
  `start_ts` timestamp NULL DEFAULT NULL,
  `end_ts` timestamp NULL DEFAULT NULL,
  `db_create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_lastupdate_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_type` (`type_id`),
  CONSTRAINT `sms_campaign_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `sms_campaign_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds data about the SMS campaigns created';

LOCK TABLES `sms_campaign` WRITE;
/*!40000 ALTER TABLE `sms_campaign` DISABLE KEYS */;

INSERT INTO `sms_campaign` (`id`, `name`, `type_id`, `start_ts`,`end_ts`)
VALUES
	(1,'Campanha sms A/B 1',2,'2019-11-01 12:00:00','2019-11-16 12:00:00'),
	(2,'Campanha sms 1',1,'2019-10-01 12:00:01','2019-10-16 12:00:01'),
	(3,'Campanha push 1',5,'2019-12-01 15:00:01','2019-12-10 09:00:01'),
	(4,'Campanha multimedia A/B 1',3,'2019-11-20 14:30:01','2019-11-25 12:00:01');
    
/*!40000 ALTER TABLE `sms_campaign` ENABLE KEYS */;
UNLOCK TABLES;

-- ------------------------------------------------------------
-- Dump of table sms_campaign_contact
-- ------------------------------------------------------------

DROP TABLE IF EXISTS `sms_campaign_contact`;

CREATE TABLE `sms_campaign_contact` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` int(10) unsigned NOT NULL,
  `sms_template_id` int(10) unsigned NOT NULL,
  `phone_prefix` smallint(3) unsigned NOT NULL COMMENT 'Destination phone prefix',
  `phone_number` int(6) unsigned NOT NULL COMMENT 'Destination phone number',
  `db_create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_lastupdate_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_sms_campaign` (`campaign_id`),
  KEY `fk_sms_template` (`sms_template_id`),
  CONSTRAINT `sms_campaign_contact_idfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `sms_campaign` (`id`),
  CONSTRAINT `sms_template_contact_idfk_1` FOREIGN KEY (`sms_template_id`) REFERENCES `sms_template` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds data about each contact of a campaign';

LOCK TABLES `sms_campaign_contact` WRITE;
/*!40000 ALTER TABLE `sms_campaign_contact` DISABLE KEYS */;

INSERT INTO `sms_campaign_contact` (`id`, `campaign_id`,`sms_template_id`, `phone_prefix`, `phone_number`)
VALUES
	(1,1,1,950,458769),
	(2,1,1,485,854756),
	(3,1,1,469,153849),
	(4,1,2,487,367259),
	(5,1,2,488,345976),
	(6,1,2,930,276481),
    (7,2,3,842,247869),
	(8,2,3,225,247968),
	(9,2,3,960,317541),
	(10,2,3,961,345796),
	(11,2,3,935,781354),
	(12,2,3,976,076873),
    (13,3,4,974,248697),
	(14,3,4,226,615978),
	(15,3,4,325,457931),
	(16,3,4,145,012976),
	(17,3,4,345,375491),
	(18,3,4,326,375498),
    (19,4,5,954,172945),
	(20,4,5,915,845976),
	(21,4,5,924,612579),
	(22,4,6,936,317295),
	(23,4,6,948,219751),
	(24,4,6,929,017295);

/*!40000 ALTER TABLE `sms_campaign_contact` ENABLE KEYS */;
UNLOCK TABLES;
-- ------------------------------------------------------------
-- Dump of table sms_campaign_send
-- ------------------------------------------------------------

DROP TABLE IF EXISTS `sms_campaign_send`;

CREATE TABLE `sms_campaign_send` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `contact_id` bigint(20) unsigned NOT NULL,
  `attempt_no` smallint(4) unsigned NOT NULL DEFAULT '1' COMMENT 'Number of the contact attempt (for resend situations)',
  `provider_id` int(10) unsigned NOT NULL,
  `network_id` int(10) unsigned NOT NULL,
  `no_messages` smallint(3) unsigned NOT NULL COMMENT 'Number of messages needed to send the content',
  `send_ts` timestamp NULL DEFAULT NULL COMMENT 'Contact send timestamp',
  `status_id` int(10) unsigned NOT NULL COMMENT 'Contact last status',
  `is_test` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Marks a test send',
  `db_create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_lastupdate_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_contact` (`contact_id`),
  KEY `fk_provider` (`provider_id`),
  KEY `fk_network` (`network_id`),
  KEY `fk_status` (`status_id`),
  CONSTRAINT `sms_campaign_send_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `sms_campaign_contact` (`id`),
  CONSTRAINT `sms_campaign_send_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `service_provider` (`id`),
  CONSTRAINT `sms_campaign_send_ibfk_3` FOREIGN KEY (`network_id`) REFERENCES `mobile_network` (`id`),
  CONSTRAINT `sms_campaign_send_ibfk_4` FOREIGN KEY (`status_id`) REFERENCES `sms_status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds data about each contact attempt processed by the system';

LOCK TABLES `sms_campaign_send` WRITE;
/*!40000 ALTER TABLE `sms_campaign_send` DISABLE KEYS */;

INSERT INTO `sms_campaign_send` (`id`, `contact_id`, `attempt_no`, `provider_id`, `network_id`, `no_messages`, `send_ts`, `status_id`, `is_test`)
VALUES
	(1,1,1,1,1,2,'2019-11-02 12:00:00',8,0),
	(2,1,2,3,1,2,'2019-11-04 12:00:00',4,0),
	(3,2,1,5,2,1,'2019-11-02 12:00:00',5,0),
	(4,3,1,2,3,1,'2019-11-02 12:00:00',5,0),
	(5,4,1,4,2,3,'2019-11-02 12:00:00',8,0),
	(6,4,2,1,2,3,'2019-11-04 12:00:00',8,0),
	(7,4,3,2,2,3,'2019-11-06 12:00:00',4,0),
	(8,5,1,1,3,3,'2019-11-02 12:02:00',8,0),
	(9,5,2,2,3,3,'2019-11-04 12:00:00',7,0),
	(10,5,3,4,3,3,'2019-11-06 12:00:00',5,0),
	(11,6,1,5,1,1,'2019-11-02 12:00:00',9,0),
    
	(12,7,1,1,4,2,'2019-10-02 12:00:00',7,0),
	(13,7,2,1,4,2,'2019-10-03 12:00:00',6,0),
	(14,8,1,5,5,1,'2019-10-02 12:00:00',5,0),
	(15,9,1,3,6,2,'2019-10-04 12:00:00',8,0),
	(16,9,2,4,6,2,'2019-10-05 12:00:00',5,0),
	(17,10,1,4,6,1,'2019-10-03 13:00:00',4,0),
	(18,11,1,2,5,1,'2019-10-02 12:00:00',4,0),
	(19,12,1,2,5,1,'2019-10-05 12:00:00',9,0),

	(20,13,1,2,6,1,'2019-12-02 15:00:01',9,0),
	(21,14,1,1,5,3,'2019-12-02 15:00:01',8,0),
	(22,14,2,4,5,3,'2019-12-03 15:00:01',7,0),
	(23,14,3,3,5,3,'2019-12-05 15:00:01',5,0),
	(24,15,1,2,4,1,'2019-12-03 15:00:01',6,0),
	(25,16,1,3,5,1,'2019-12-02 15:00:01',4,0),
	(26,17,1,5,6,1,'2019-12-03 15:00:01',5,0),
	(27,18,1,5,4,1,'2019-12-04 15:00:01',4,0),

	(28,19,1,2,6,2,'2019-11-20 15:30:01',8,0),
	(29,19,2,3,2,2,'2019-11-23 14:30:01',4,0),
	(30,20,5,4,5,1,'2019-11-22 14:30:01',5,0),
	(31,21,4,3,5,1,'2019-11-21 14:30:01',5,0),
	(32,22,3,2,1,3,'2019-11-21 12:30:01',8,0),
	(33,22,2,3,1,3,'2019-11-22 14:30:01',7,0),
	(34,22,3,5,1,3,'2019-11-23 14:30:01',5,0),
	(35,23,2,5,4,1,'2019-11-23 09:30:01',6,0),
	(36,24,1,5,3,2,'2019-11-22 10:30:01',8,0),
	(37,24,2,5,3,2,'2019-11-23 14:30:01',4,0);

/*!40000 ALTER TABLE `sms_campaign_send` ENABLE KEYS */;
UNLOCK TABLES;
-- ------------------------------------------------------------
-- Dump of table sms_campaign_send_status
-- ------------------------------------------------------------

DROP TABLE IF EXISTS `sms_campaign_send_status`;

CREATE TABLE `sms_campaign_send_status` (
  `send_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `status_id` int(10) unsigned NOT NULL,
  `status_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`send_id`,`status_id`),
  KEY `fk_status` (`status_id`),
  CONSTRAINT `sms_campaign_send_status_ibfk_1` FOREIGN KEY (`send_id`) REFERENCES `sms_campaign_send` (`id`),
  CONSTRAINT `sms_campaign_send_status_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `sms_status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds data about every state each contact passes through';

LOCK TABLES `sms_campaign_send_status` WRITE;
/*!40000 ALTER TABLE `sms_campaign_send_status` DISABLE KEYS */;

INSERT INTO `sms_campaign_send_status` (`send_id`, `status_id`, `status_ts`)
VALUES
	(1,8,'2019-11-02 12:01:00'),
	(2,4,'2019-11-04 12:00:45'),
	(3,5,'2019-11-02 12:00:10'),
	(4,5,'2019-11-02 12:00:15'),
	(5,8,'2019-11-02 12:00:35'),
	(6,8,'2019-11-04 12:00:54'),
	(7,4,'2019-11-06 12:00:15'),
	(8,7,'2019-11-02 12:02:25'),
	(9,5,'2019-11-04 12:00:34'),
	(10,9,'2019-11-06 12:00:15'),
	(11,7,'2019-11-02 12:00:49'),
    
    (12,7,'2019-10-02 12:00:58'),
	(13,6,'2019-10-03 12:00:19'),
	(14,5,'2019-10-02 12:00:58'),
	(15,8,'2019-10-04 12:00:34'),
	(16,5,'2019-10-05 12:00:15'),
	(17,4,'2019-10-03 13:00:04'),
	(18,4,'2019-10-02 12:00:46'),
	(19,9,'2019-10-05 12:00:37'),

	(20,9,'2019-12-02 15:00:42'),
	(21,8,'2019-12-02 15:00:49'),
	(22,7,'2019-12-03 15:00:46'),
	(23,5,'2019-12-05 15:00:23'),
	(24,6,'2019-12-03 15:00:55'),
	(25,4,'2019-12-02 15:00:34'),
	(26,5,'2019-12-03 15:00:32'),
	(27,4,'2019-12-04 15:00:52'),

	(28,8,'2019-11-20 15:30:43'),
	(29,4,'2019-11-23 14:30:47'),
	(30,5,'2019-11-22 14:30:27'),
	(31,5,'2019-11-21 14:30:34'),
	(32,8,'2019-11-21 12:30:28'),
	(33,7,'2019-11-22 14:30:29'),
	(34,5,'2019-11-23 14:30:37'),
	(35,6,'2019-11-23 09:30:15'),
	(36,8,'2019-11-22 10:30:47'),
	(37,4,'2019-11-23 14:30:51');

/*!40000 ALTER TABLE `sms_campaign_send_status` ENABLE KEYS */;
UNLOCK TABLES;
-- ------------------------------------------------------------
-- Dump of table sms_campaign_type
-- ------------------------------------------------------------

DROP TABLE IF EXISTS `sms_campaign_type`;

CREATE TABLE `sms_campaign_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_ab` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'the campaign has A/B testing',
  `is_push` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'the campaign messages are sent as push notifications',
  `is_multimedia` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'the campaign has multimedia content',
  `db_create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_lastupdate_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds data about the types of campaigns available';

LOCK TABLES `sms_campaign_type` WRITE;
/*!40000 ALTER TABLE `sms_campaign_type` DISABLE KEYS */;

INSERT INTO `sms_campaign_type` (`id`, `is_ab`, `is_push`, `is_multimedia`)
VALUES
	(1,0,0,0),
	(2,1,0,0),
	(3,1,0,1),
	(4,1,1,0),
	(5,0,1,0),
	(6,0,0,1);

/*!40000 ALTER TABLE `sms_campaign_type` ENABLE KEYS */;
UNLOCK TABLES;

-- ------------------------------------------------------------
-- Dump of table sms_cost
-- ------------------------------------------------------------

DROP TABLE IF EXISTS `sms_cost`;

CREATE TABLE `sms_cost` (
  `provider_id` int(10) unsigned NOT NULL,
  `network_id` int(10) unsigned NOT NULL,
  `campaign_type_id` int(10) unsigned NOT NULL,
  `cost_per_message` float NOT NULL DEFAULT '0' COMMENT 'Cost of each message sent from a provider to a network',
  `db_create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_lastupdate_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`provider_id`,`network_id`,`campaign_type_id`),
  KEY `fk_network` (`network_id`),
  KEY `fk_campaign_type` (`campaign_type_id`),
  CONSTRAINT `sms_cost_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `service_provider` (`id`),
  CONSTRAINT `sms_cost_ibfk_2` FOREIGN KEY (`network_id`) REFERENCES `mobile_network` (`id`),
  CONSTRAINT `sms_cost_ibfk_3` FOREIGN KEY (`campaign_type_id`) REFERENCES `sms_campaign_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds data used to calculate the cost of a message, depending on the service provider, destination network and campaign type';

LOCK TABLES `sms_cost` WRITE;
/*!40000 ALTER TABLE `sms_cost` DISABLE KEYS */;

INSERT INTO `sms_cost` (`provider_id`, `network_id`, `campaign_type_id`, `cost_per_message`)
VALUES
	(1,1,2,0.07),
	(3,1,2,0.1),
	(5,2,2,0.12),
	(4,2,2,0.06),
	(1,2,2,0.08),
    (2,2,2,0.11),
	(1,3,2,0.09),
	(2,3,2,0.1),
	(4,3,2,0.13),
	(5,1,2,0.05),
    
	(1,4,1,0.09),
    (5,5,1,0.1),
	(3,6,1,0.11),
	(4,6,1,0.0),
	(2,5,1,0.08),
    
	(2,6,5,0.01),
	(1,5,5,0.01),
    (4,5,5,0.01),
	(3,5,5,0.01),
	(2,4,5,0.01),
	(5,6,5,0.01),
	(5,4,5,0.01),
    
	(2,6,3,0.27),
    (3,2,3,0.15),
	(4,5,3,0.2),
	(3,5,3,0.24),
	(2,1,3,0.22),
	(3,1,3,0.17),
	(5,1,3,0.19),
    (5,4,3,0.3),
	(5,3,3,0.26);

/*!40000 ALTER TABLE `sms_cost` ENABLE KEYS */;
UNLOCK TABLES;
-- ------------------------------------------------------------
-- Dump of table sms_status
-- ------------------------------------------------------------

DROP TABLE IF EXISTS `sms_status`;

CREATE TABLE `sms_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `is_success` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'If this state represents a sucessfully sent and received message',
  `is_billable` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'If this state represents a message that should be billed to the client',
  `is_final` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'If this state represents a message in its final state (that won''t change anymore)',
  `db_create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_lastupdate_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SMS lifecycle state machine';

LOCK TABLES `sms_status` WRITE;
/*!40000 ALTER TABLE `sms_status` DISABLE KEYS */;

INSERT INTO `sms_status` (`id`, `name`, `is_success`, `is_billable`, `is_final`)
VALUES
	(1,'ready',0,0,0),
	(2,'queued',0,0,0),
	(3,'awaiting_confirmation',0,1,0),
	(4,'delivered',1,1,1),
	(5,'read',1,1,1),
	(6,'blacklisted',0,0,1),
	(7,'insufficient_balance',0,0,1),
	(8,'soft_bounce',0,1,1),
	(9,'hard_bounce',0,1,1);

/*!40000 ALTER TABLE `sms_status` ENABLE KEYS */;
UNLOCK TABLES;

-- ------------------------------------------------------------
-- Dump of table sms_template
-- ------------------------------------------------------------

DROP TABLE IF EXISTS `sms_template`;

CREATE TABLE `sms_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `a_or_b` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 -> "Regular" template; 1 -> "A" template; 2 -> "B" template',
  `content` text COMMENT 'Text content of a message',
  `media_content` blob COMMENT 'Multimedia content of a message',
  `db_create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_lastupdate_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds data about campaign templates (if campaign is A/B, each campaign will have two templates)';

LOCK TABLES `sms_template` WRITE;
/*!40000 ALTER TABLE `sms_template` DISABLE KEYS */;

INSERT INTO `sms_template` (`id`, `campaign_id`, `name`, `a_or_b`, `content`, `media_content`)
VALUES
	(1,1,'Campanha sms A/B 1A',1,'texto da Campanha sms A/B 1A',null),
	(2,1,'Campanha sms A/B 1B',2,'texto da Campanha sms A/B 1B',null),
	(3,2,'Campanha sms 1',0,'texto da Campanha sms 1',null),
	(4,3,'Campanha push 1',0,'texto da Campanha push 1',null),
	(5,4,'Campanha multimedia A/B 1A',1,'texto da Campanha multimedia A/B 1A','imagem1'),
	(6,4,'Campanha multimedia A/B 1B',2,'texto da Campanha multimedia A/B 1B','imagem2');

/*!40000 ALTER TABLE `sms_template` ENABLE KEYS */;
UNLOCK TABLES;


SET SESSION FOREIGN_KEY_CHECKS = 1;
