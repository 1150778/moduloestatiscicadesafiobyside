package Services;

import MainMenu.Main;
import scriptella.execution.EtlExecutor;
import scriptella.execution.EtlExecutorException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Paths;

public class CampanhaMaiorCustoPorContactoService {
    private final String FILE_NAME = "xml/CampanhaMaiorCustoPorContacto.xml";

    public CampanhaMaiorCustoPorContactoService() {
    }

    public void CampanhaMaiorCustoPorContacto()  {
        String csvFile="";

        ClassLoader classLoader = new Main().getClass().getClassLoader();

        File file = new File(classLoader.getResource(FILE_NAME).getFile());

        try {
            EtlExecutor.newExecutor(file).execute();
        }catch(EtlExecutorException e){
            e.printStackTrace();
        }

        try {
            csvFile = Paths.get("target/classes/xml", "/CampanhaMaiorCustoPorContacto.csv").toUri().toURL().getFile();
        }catch(MalformedURLException e){
            e.printStackTrace();
        }

        String line = "";
        String cvsSplitBy = ",";
        try (BufferedReader reader = new BufferedReader(new FileReader(csvFile))) {

            while ((line = reader.readLine()) != null) {

                String[] result = line.split(cvsSplitBy);
                System.out.println("A Campanha " + result[0] + " com o id " + result[1] + " teve o maior custo por contacto com mensagem paga de " + result[2] + " euros");
                break;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
