package MainMenu;

import Services.*;

import java.util.Scanner;

public class MainMenu {

    public static void mainMenu() {
            System.out.println("Escolha uma das opções");
            System.out.println("1- Numero medio de mensagens por campanha");
            System.out.println("2- campanha com maior custo absoluto");
            System.out.println("3- campanha com maior custo por contacto");
            System.out.println("4- campanha com maior custo por mensagem");
            System.out.println("5- campanha enviada para o maior numero de paises");
            System.out.println("6- Versão vencedora para cada uma das campanhas A/B");
            System.out.println("7- Campanha com o maior taxa de sucesso nos reenvios");
            System.out.println("8- Razões mais comuns para o insucesso");
            System.out.println("9- Tempo medio de confirmação das mensagens");
            System.out.println("10- Qual a campanha com a pior performance");
            System.out.println("11- Mostrar Opções de Novo");
            System.out.println("0- Fechar Aplicação");
            Scanner scanner = new Scanner(System.in);
            String escolha;
            boolean repeat=true;
            while(repeat){
                System.out.println("Insira a sua opção");
                escolha=scanner.nextLine();
                escolha=escolha.trim();
                switch(escolha){
                    case "1":
                        NumeroMedioMensagensService numeroMedioMensagensService = new NumeroMedioMensagensService();
                        numeroMedioMensagensService.numeroMedioMensagensPorCampanha();
                        break;
                    case "2":
                        CampanhaMaiorCustoAbsolutoService campanhaMaiorCustoAbsolutoService = new CampanhaMaiorCustoAbsolutoService();
                        campanhaMaiorCustoAbsolutoService.CampanhaMaiorCustoAbsoluto();
                        break;
                    case "3":
                        CampanhaMaiorCustoPorContactoService campanhaMaiorCustoPorContactoService = new CampanhaMaiorCustoPorContactoService();
                        campanhaMaiorCustoPorContactoService.CampanhaMaiorCustoPorContacto();
                        break;
                    case "4":
                        CampanhaMaiorCustoPorMensagemService campanhaMaiorCustoPorMensagemService = new CampanhaMaiorCustoPorMensagemService();
                        campanhaMaiorCustoPorMensagemService.CampanhaMaiorCustoPorMensagem();
                        break;
                    case "5":
                        CampanhaEnviadaMaisPaisesServices campanhaEnviadaMaisPaisesServices = new CampanhaEnviadaMaisPaisesServices();
                        campanhaEnviadaMaisPaisesServices.CampanhaEnviadaMaisPaises();
                        break;
                    case "6":
                        System.out.println("Função não implementada!!");
                        break;
                    case "7":
                        TaxaSucessoReenviosService taxaSucessoReenviosService = new TaxaSucessoReenviosService();
                        taxaSucessoReenviosService.TaxaSucessoReenvios();
                        break;
                    case "8":
                        CausaComunsInsucessoService causaComunsInsucessoService = new CausaComunsInsucessoService();
                        causaComunsInsucessoService.CausaComunsInsucesso();
                        break;
                    case "9":
                        TempoMedioMensagensService tempoMedioMensagensService = new TempoMedioMensagensService();
                        tempoMedioMensagensService.tempoMedioMensagens();
                        break;
                    case "10":
                        PiorCampanhaService piorCampanhaService = new PiorCampanhaService();
                        piorCampanhaService.PiorCampanha();
                        break;
                    case "11":
                        MainMenu.mainMenu();
                        repeat=false;
                        break;
                    case "0":
                        repeat=false;
                        break;
                    default:
                        System.out.println("Opção Invalida");
                        break;
                }
            }

    }
}
